//Server Variable Declarations
var express = require('express'),
    User    = require('./api/models/user'),
    Item    = require('./api/models/item'),
    bodyParser = require('body-parser'),
    passport    = require('passport'),
    LocalStrategy = require('passport-local'),
    PayPalStrategy = require('passport-paypal'),
    mongoose = require('mongoose'),
    $ = require('jquery'),
    app     = express();

//Routes
var indexRoutes = require('./api/routes/index'),
    userRoutes  = require('./api/routes/users'),
    itemRoutes  = require('./api/routes/items'),
    fixRoutes   = require('./api/routes/repairs/fix'),
    shopRoutes  = require('./api/routes/shop/shop');
    freelanceRoutes = require('./api/routes/freelance');
mongoose.connect("mongodb://localhost/phone-garage");

app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({extended:true}));
app.use(express.static(__dirname+"/public"));

//app.use(itemRoutes);
//app.use(userRoutes);
app.use(shopRoutes);
app.use(freelanceRoutes);
app.use(fixRoutes);
app.use(indexRoutes);



app.listen(3000, function(){
    console.log("Server started")
});
