var express= require('express'),
    router  = express.Router({mergeParams:true}),
    repairCtrl = require('../../controllers/fix.controller'),
    Fix = require('../../models/fix');

router.get("/fix", function(req,res){
    res.render("new-request");
});

router.post("/fix", repairCtrl.addRepairRequest);


module.exports = router;
