var express= require('express'),
    router  = express.Router(),
    freelanceCtrl = require('../controllers/freelance.controller')

router.get("/freelance", function(req,res){
    res.render("freelance");
});
router.post("/freelance", freelanceCtrl.addNewFreelanceReq);

module.exports= router;
