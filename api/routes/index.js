var express = require('express'),
    router  = express.Router({mergeParams:true});



router.get("/", function(req,res){
    res.render("index");
});

router.get("*", function(req,res){
    res.render("error");
});

module.exports = router;
