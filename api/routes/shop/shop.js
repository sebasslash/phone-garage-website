var express = require('express'),
    router  = express.Router({mergeParams:true});

module.exports = router;

router.get('/shop', function(req,res){
    res.render("shop");
});
