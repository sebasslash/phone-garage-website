var mongoose = require('mongoose');


var fixSchema = new mongoose.Schema({
    name:String,
    phone:String,
    phoneModel:String
});


module.exports = mongoose.model("Fix", fixSchema);
